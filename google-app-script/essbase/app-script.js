
function startScript() {
  ScriptApp.newTrigger("refreshData")
    .timeBased()
    .everyHours(1)
    .create();
}

function logout(apikey, userid) {
  let payload = JSON.stringify({
    "loginid": userid
  });

  let options = {
    method: "post",
    contentType: "application/json",
    headers: {
      connector: "essbase",
      qibateskey: apikey
    },
    payload,
    muteHttpExceptions: true,
  },

    response = UrlFetchApp.fetch("https://qibates-api.quartz-insight.com/qibates/api/v2/action/logout", options);
  return response.getContentText("UTF-8");
}

function getLoginId(apikey) {

  let payload = JSON.stringify({
    "username": "qi",
    "password": "*********",
    "providerAddress": "https://yourdomain/aps/JAPI"
  });

  let options = {
    method: "post",
    contentType: "application/json",
    headers: {
      connector: "essbase",
      qibateskey: apikey
    },
    payload,
    muteHttpExceptions: true,
  },

    response = UrlFetchApp.fetch("https://qibates-api.quartz-insight.com/qibates/api/v2/action/login", options);
  let loginid = JSON.parse(response.getContentText("UTF-8"));
  return loginid.loginid;
}

function refreshData() {

  const apikey = "*******-****-****-****-**********";

  let loginid = getLoginId(apikey);

  let ss = SpreadsheetApp.getActiveSpreadsheet();
  let sheet = ss.getSheetByName("rest-api");
  let rangeValues = sheet.getDataRange().getValues();


  let payload = JSON.stringify(
    {
      "loginid": loginid,
      "server": "ecs-epmdev03",
      "application": "QiBates",
      "cube": "Demo",
      "aliastable": "",
      "action": {
        "grid": rangeValues
      },

      "settings": {
        "missingSuppression": false,
        "missingLabel": "0",
        "locale": "fr_FR",
        "navigateWithoutData": false,
        "withoutIndentation": true
      }
    }
  );

  let options = {
    method: "post",
    contentType: "application/json",
    headers: {
      connector: "essbase",
      qibateskey: apikey
    },
    payload,
    muteHttpExceptions: true,
  },

    response = UrlFetchApp.fetch("https://qibates-api.quartz-insight.com/qibates/api/v2/action/refresh", options);
  let newValues = JSON.parse(response.getContentText("UTF-8")).values;
  let newRange = sheet.getRange(
    1,
    1,
    newValues.length,
    newValues[0].length
  );

  newRange.setValues(newValues);

  logout(apikey, loginid);
}



