
function showInSheet(sheet, grid) {

    sheet.getDataRange().clearContent();

    let range = sheet.getRange(
        1,
        1,
        grid.values.length,
        grid.values[0].length
    );


    processGridData(grid);
    setNumberStringFormat(grid, range, sheet);
    range.clearDataValidations();
    range.setValues(grid.values);
    setDataValidation(
        grid.dataValidationRanges,
        grid.smartlists,
        sheet
    );
    setRangeNotes(range, grid, sheet);
}

// updating values based on smartlists and applying time zone to date cell.
function processGridData(grid) {
    try {
        let values = grid.values;

        for (let i = 0; i < grid.dataValidationRanges.length; i++) {
            for (
                let row = grid.dataValidationRanges[i].startRow;
                row < grid.dataValidationRanges[i].startRow + grid.dataValidationRanges[i].numRows;
                row++
            ) {
                for (
                    let col = grid.dataValidationRanges[i].startCol;
                    col < grid.dataValidationRanges[i].startCol + grid.dataValidationRanges[i].numCols;
                    col++
                ) {
                    if (grid.dataValidationRanges[i].typeId != "date") {
                        let index = parseInt(values[row][col]);
                        if (!isNaN(index) && index > 0) {
                            let smartListEntries = grid.smartlists.find(
                                (x) => x.typeId === grid.dataValidationRanges[i].typeId
                            ).listEntries;
                            let value = smartListEntries.find((entry) => Object.keys(entry)[0] === String(index));
                            if (value) {
                                values[row][col] = Object.values(value)[0];
                            }
                        }
                    } else {
                        let date = values[row][col].split("/");
                        let day = date[0];
                        let month = date[1];
                        let year = date[2];

                        if (month != undefined && year != undefined) {
                            let cellDate = getDateWithTimeZone(new Date(year, month - 1, day));
                            values[row][col] = cellDate;
                        }
                    }
                }
            }
        }

        return values;
    } catch (error) {
        console.error("Error in processGridData: " + error);
    }
}

function getDateWithTimeZone(cellDate) {
    let timeZone = Session.getScriptTimeZone();
    let sheetTimeZone =
        SpreadsheetApp.getActiveSpreadsheet().getSpreadsheetTimeZone();
    let sessionTZ = Utilities.formatDate(cellDate, timeZone, "yyyyMMddHHmmss");
    let spreadsheetTZ = Utilities.formatDate(
        cellDate,
        sheetTimeZone,
        "yyyyMMddHHmmss"
    );
    let milliSession = new Date(
        sessionTZ.substring(0, 4),
        sessionTZ.substring(4, 6),
        sessionTZ.substring(6, 8),
        sessionTZ.substring(8, 10),
        sessionTZ.substring(10, 12),
        sessionTZ.substring(12, 14)
    ).getTime();
    let milliSpreadsheet = new Date(
        spreadsheetTZ.substring(0, 4),
        spreadsheetTZ.substring(4, 6),
        spreadsheetTZ.substring(6, 8),
        spreadsheetTZ.substring(8, 10),
        spreadsheetTZ.substring(10, 12),
        spreadsheetTZ.substring(12, 14)
    ).getTime();

    let diff = milliSession - milliSpreadsheet ;

    return new Date(cellDate.getTime() + diff) ;
}

function setNumberStringFormat(grid, targetRange, activeSheet) {

    let protections = activeSheet.getProtections(SpreadsheetApp.ProtectionType.RANGE);
    for (let i = 0; i < protections.length; i++) {
        let protection = protections[i];
        if (protection.canEdit()) { protection.remove() }
    }

    for (let i = 0; i < grid.types["MEMBER"].length; i++) {
        let range = activeSheet.getRange(
            grid.types["MEMBER"][i].startRow + 1 + (grid.range ? grid.range.startRow : targetRange.getRow() - 1),
            grid.types["MEMBER"][i].startCol + 1 + (grid.range ? grid.range.startCol : targetRange.getColumn() - 1),
            grid.types["MEMBER"][i].numRows,
            grid.types["MEMBER"][i].numCols
        );

        range.setNumberFormat('@');
        range.setBackground('#bedaff').setFontColor('#000000')
        range.protect().setWarningOnly(true)
    }

    for (let i = 0; i < grid.types["READWRITE"].length; i++) {
        let range = activeSheet.getRange(
            grid.types["READWRITE"][i].startRow + 1 + (grid.range ? grid.range.startRow : targetRange.getRow() - 1),
            grid.types["READWRITE"][i].startCol + 1 + (grid.range ? grid.range.startCol : targetRange.getColumn() - 1),
            grid.types["READWRITE"][i].numRows,
            grid.types["READWRITE"][i].numCols
        );
        range.setBackground('#fffeb7').setFontColor('#000000')
    }

    for (let i = 0; i < grid.types["READONLY"].length; i++) {
        let range = activeSheet.getRange(
            grid.types["READONLY"][i].startRow + 1 + (grid.range ? grid.range.startRow : targetRange.getRow() - 1),
            grid.types["READONLY"][i].startCol + 1 + (grid.range ? grid.range.startCol : targetRange.getColumn() - 1),
            grid.types["READONLY"][i].numRows,
            grid.types["READONLY"][i].numCols
        );
        range.setBackground('#d6d6d6').setFontColor('#000000')
        range.protect().setWarningOnly(true)
    }
}

function setRangeNotes(range, grid, sheet) {
    try {
        if (grid.cellnotes != null) {
            let cellNotes = removeNoteEqualToValue(grid);
            range.setNotes(cellNotes);
        }
        let cell = sheet.getRange("A1");
        let povNote = getPovAndConnectionNote(grid.pov, sheet);
        cell.setNote(povNote);
    } catch (error) {
        console.error({
            message: "Error in setRangeNotes",
            error: error,
            data: grid.cellnotes,
        });
    }
}

function removeNoteEqualToValue(grid) {
    try {
        for (let x = 0; x < grid.cellnotes.length; x++) {
            for (let y = 0; y < grid.cellnotes[0].length; y++) {
                if (grid.cellnotes[x][y] == grid.values[x][y]) {
                    grid.cellnotes[x][y] = '';
                }
            }
        }
        return grid.cellnotes;
    } catch (error) {
        console.error({
            message: "Error in removeNoteEqualToValue",
            error: error,
            data: grid.cellnotes,
        });
        return grid.cellnotes;
    }
}


function getPovAndConnectionNote(pov, sheet) {
    try {
        let gridsMetadata = getSheetMeta(sheet, "Gridskey");
        let povNote = "";
        if (Object.keys(pov).length === 0) {
            console.log("Notes is empty");
        } else {
            for (let key in pov) {
                povNote += pov[key].dim + ": " + pov[key].value + "\n";
            }
        }
        if (Object.keys(gridsMetadata).length !== 0) {
            let formPath = gridsMetadata["1:1"].form.split("/");
            let formName = formPath.pop();
            let formFolder = formPath.join("/");
            povNote +=
                "---\n" +
                "Application: " +
                gridsMetadata["1:1"].application +
                "\n" +
                "Form path: /" +
                formFolder +
                "/\n" +
                "Form name: " +
                formName +
                "\n";
            if (gridsMetadata["1:1"].aliastable) {
                povNote += "Aliastable: " + gridsMetadata["1:1"].aliastable + "\n";
            }
        }
        return povNote;
    } catch (error) {
        console.error({
            message: "Error in getPovAndConnectionNote",
            error: error,
            data: pov,
        });
    }
}


function setDataValidation(
    dataValidationRanges,
    smartlists,
    activeSheet
) {
    if (dataValidationRanges == null) {
        console.log("dataRules is empty");
    } else {
        let missingLabel = "#Missing";
        for (let i = 0; i < dataValidationRanges.length; i++) {
            let range = activeSheet.getRange(
                dataValidationRanges[i].startRow + 1,
                dataValidationRanges[i].startCol + 1,
                dataValidationRanges[i].numRows,
                dataValidationRanges[i].numCols
            );
            if (dataValidationRanges[i].typeId == "date") {
                range.setDataValidation(
                    SpreadsheetApp.newDataValidation()
                        .setAllowInvalid(false)
                        .requireDate()
                        .setHelpText('valide date required')
                        .build()
                );
            } else {
                var smartListEntries = smartlists.find((x) => x.typeId === dataValidationRanges[i].typeId).listEntries;
                let smartList = smartListEntries.map(entry => Object.values(entry)[0]);
                smartList.push(missingLabel);
                range.setDataValidation(
                    SpreadsheetApp.newDataValidation().requireValueInList(smartList, true).setAllowInvalid(false)
                );
            }
        }
    }
}


function getSheetMeta(sheet, key) {
    let metadatas = sheet.createDeveloperMetadataFinder().withKey(key).find();

    if (metadatas.length === 0) {
        throw new Error("meta not found in - " + sheet.getName());
    }
    let metadata = metadatas[0].getValue();
    if (!metadata || metadata === 'null') {
        for (let i = 0; i < metadatas.length; i++) {
            metadatas[i].remove();
        }
        throw new Error("meta not found in - " + sheet.getName());
    }
    return JSON.parse(metadata);
}


function setSheetMeta(sheet, key, value) {
    let jsonValue = JSON.stringify(value);
    try {
        let metadata = sheet.createDeveloperMetadataFinder().withKey(key).find();
        if (metadata.length == 0) {
            sheet.addDeveloperMetadata(key, jsonValue);
        } else {
            metadata[0].setValue(jsonValue);
        }
    }
    catch (error) {
        console.error("Error in setMetadata: " + error);
    }
}

function getPov(sheet) {
    try {
        return getSheetMeta(sheet, "pov")
    } catch (error) {
        console.error("Error in getPov: " + error + "\n use default pov");
        return {};
    }
}

function getFormMeta(sheet) {
    return getSheetMeta(sheet, "Gridskey")["1:1"];
}

function updateCodeMeta(grid) {
    let codes = compressObject(grid.codes);
    let gridskey = getSheetMeta(sheet, "Gridskey");
    gridskey["1:1"].codes = codes;

    if (!hasEnoughSpaceForMetadata(gridskey, "Gridskey")) {
        if (gridskey["1:1"].codes) {
            gridskey["1:1"].codes = null;
        }
        console.error("The form exceeds Google's quotas. To submit the form, please reduce the form size.");
    }
    setSheetMeta(sheet, "Gridskey", gridskey);
}

function getGridWithDateFormat(activeSheet, spreadsheet) {
    try {
        let grid = activeSheet.getDataRange().getValues();
        for (let row = 0; row < grid.length; row++) {
            for (let col = 0; col < grid[row].length; col++) {
                if (grid[row][col] instanceof Date) {
                    grid[row][col] = Utilities.formatDate(
                        grid[row][col],
                        spreadsheet.getSpreadsheetTimeZone(),
                        "dd/MM/yyyy"
                    );
                } else if (typeof grid[row][col] == "string") {
                    const regex = /^'/i;
                    grid[row][col] = grid[row][col].replace(regex, "");
                }
            }
        }
        return grid;
    } catch (error) {
        console.error("Error in getGridWithDateFormat : " + error);
        return [[]];
    }
}

function getCodes(sheet) {
    let codes = null;
    let grid = getFormMeta(sheet)
    if (grid && grid.codes) {
        codes = grid.codes;
    }
    try {
        if (codes) {
            codes = uncompressObject(codes);
        }
    } catch (Exception) {
        codes = null;
    }
    return codes;
}

function uncompressObject(compressed) {
    let todecompress = Utilities.newBlob(Utilities.base64Decode(compressed), "application/x-gzip");
    let z = Utilities.ungzip(todecompress);
    return JSON.parse(z.getDataAsString());
}

function compressObject(source) {
    let blob = Utilities.newBlob(JSON.stringify(source));
    let compressedblob = Utilities.gzip(blob);
    return Utilities.base64Encode(compressedblob.getBytes());
}

function hasEnoughSpaceForMetadata(gridsMetadata, metaKey) {
    let size = 0;
    activeSheet.createDeveloperMetadataFinder().find().forEach(p => {
        if (p.getKey() !== metaKey)
            size = size + p.getKey().length + p.getValue().length;
    });
    size = JSON.stringify(gridsMetadata).length + metaKey.length;
    return size <= 30000;
}