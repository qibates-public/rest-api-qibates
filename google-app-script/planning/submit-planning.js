function main_submit() {
    console.time("submitData");
    submitData();
    console.timeEnd("submitData");
  }

  function logout(apikey, userid) {
    let payload = JSON.stringify({
      "loginid": userid
    });
  
    let options = {
      method: "post",
      contentType: "application/json",
      headers: {
        connector: "planning",
        qibateskey: apikey
      },
      payload,
      muteHttpExceptions: true,
    },
  
      response = UrlFetchApp.fetch("https://qibates-api.quartz-insight.com/qibates/api/v2/action/logout", options);
    return response.getContentText("UTF-8");
  }
  
  
  
  function getLoginId(apikey) {
  
    let payload = JSON.stringify({
        "username": "*******",
        "password": "***********",
        "providerAddress": "https://*******/HyperionPlanning/SmartView"
    });
  
    let options = {
      method: "post",
      contentType: "application/json",
      headers: {
        connector: "planning",
        qibateskey: apikey
      },
      payload,
      muteHttpExceptions: true,
    },
  
      response = UrlFetchApp.fetch("https://qibates-api.quartz-insight.com/qibates/api/v2/action/login", options);
    let loginid = JSON.parse(response.getContentText("UTF-8"));
    return loginid.loginid;
  }
  
  function generatePayload(loginid, spreadsheet, sheet) {
    let gridmeta = getFormMeta(sheet);
    let formPath = gridmeta.form.split("/");
    let formName = formPath.pop();
    let formFolder = formPath.join("/");
  
  
    let payload =
    {
      "loginid": loginid,
      "server": gridmeta.server,
      "application": gridmeta.application,
      "folder": formFolder,
      "formName": formName,
      "type": "regular",
      "preferences": {
        "missingColSuppression": false,
        "missingRowSuppression": false,
        "missingLabel": "#Missing",
        "locale": spreadsheet.getSpreadsheetLocale()
      },
      "pov": getPov(sheet),
      "grid" : getGridWithDateFormat(sheet, spreadsheet),
      "codes" : getCodes(sheet)
    }
      ;
    return JSON.stringify(payload);
  }
  
  function submitData() {
    const apikey = "*************************************************";
    let loginid = getLoginId(apikey);
    let spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    let sheet = spreadsheet.getActiveSheet();
    let payload = generatePayload(loginid, spreadsheet, sheet);
  
    let options = {
      method: "post",
      contentType: "application/json",
      headers: {
        connector: "planning",
        qibateskey: apikey
      },
      payload,
      muteHttpExceptions: true,
    },
  
      response = UrlFetchApp.fetch("https://qibates-api.quartz-insight.com/qibates/api/v2/action/submitform", options);
    let newValues = JSON.parse(response.getContentText("UTF-8"));
  
  
    let grid = newValues.subforms[0] ;
  
    showInSheet(sheet, grid);

    logout(apikey, loginid);
  
  }
  
  